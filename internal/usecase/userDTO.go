package usecase

import (
	"fmt"

	"gitlab.com/zaur.ashurbekov/authenticator/internal/entity"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID       string
	Email    string
	Username string
	IsAdmin  bool
}

type AddUserIn struct {
	Email    string
	Username string
	Password string
	IsAdmin  bool
}

type UpdateUserIn struct {
	ID       string
	Email    string
	Username string
	Password string
	IsAdmin  bool
}

func (i *AddUserIn) toNewUserEntity() (*entity.User, error) {
	u, err := entity.NewUser()
	if err != nil {
		return nil, fmt.Errorf("can't create new user entity: %w", err)
	}

	hashPass, err := hashPassword(i.Password)
	if err != nil {
		return nil, fmt.Errorf("can't hash password: %w", err)
	}

	u.Email = i.Email
	u.Username = i.Username
	u.Password = hashPass
	u.IsAdmin = i.IsAdmin

	return &u, nil
}

func (i *UpdateUserIn) toUserEntity() (*entity.User, error) {
	hashPass, err := hashPassword(i.Password)
	if err != nil {
		return nil, fmt.Errorf("can't hash password: %w", err)
	}

	u := entity.User{
		ID:       i.ID,
		Email:    i.Email,
		Username: i.Username,
		Password: hashPass,
		IsAdmin:  i.IsAdmin,
	}

	return &u, nil
}

func fromEntityUser(u *entity.User) *User {
	return &User{
		ID:       u.ID,
		Email:    u.Email,
		Username: u.Username,
		IsAdmin:  u.IsAdmin,
	}
}

func fromEntityUsers(users []entity.User) []User {
	res := make([]User, 0, len(users))

	for _, u := range users {
		res = append(res, *fromEntityUser(&u))
	}

	return res
}

func hashPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(hash), err
}

func isPasswordHash(password, hash string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password)) == nil
}

package usecase

type UserConfig struct {
	MaxPaginationListSize     int `envconfig:"max_pagination_list_size" default:"100"`
	DefaultPaginationListSize int `envconfig:"max_pagination_list_size" default:"10"`
}

package usecase

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/zaur.ashurbekov/authenticator/internal/entity"
	"gitlab.com/zaur.ashurbekov/authenticator/internal/util"
)

var (
	ErrAccessDenied = errors.New("access denied")
)

type UserRepo interface {
	Add(ctx context.Context, u entity.User) error
	GetByID(ctx context.Context, ID string) (*entity.User, error)
	GetByUsername(ctx context.Context, username string) (*entity.User, error)
	GetList(ctx context.Context, fromID *string, limit int) ([]entity.User, error)
	Update(ctx context.Context, u entity.User) error
	DeleteByID(ctx context.Context, ID string) error
}

type Users struct {
	repo                      UserRepo
	MaxPaginationListSize     int
	DefaultPaginationListSize int
}

func NewUsers(cfg UserConfig, userRepo UserRepo) *Users {
	return &Users{
		repo:                  userRepo,
		MaxPaginationListSize: cfg.MaxPaginationListSize,
	}
}

func (s *Users) AddUser(ctx context.Context, in AddUserIn) (*User, error) {
	if ok := util.HasAdminRights(ctx); !ok {
		return nil, ErrAccessDenied
	}

	u, err := in.toNewUserEntity()
	if err != nil {
		return nil, err
	}

	if err := u.Validate(); err != nil {
		return nil, fmt.Errorf("invalid user: %w", err)
	}

	err = s.repo.Add(ctx, *u)
	if err != nil {
		return nil, fmt.Errorf("can't add user to repo: %w", err)
	}

	return fromEntityUser(u), nil
}

func (s *Users) GetByID(ctx context.Context, ID string) (*User, error) {

	u, err := s.repo.GetByID(ctx, ID)
	if err != nil {
		return nil, fmt.Errorf("can't add user to repo: %w", err)
	}

	return fromEntityUser(u), nil
}

func (s *Users) GetList(ctx context.Context, fromID *string, limit *int) ([]User, error) {
	if limit == nil {
		limit = &s.DefaultPaginationListSize
	}

	if *limit > s.MaxPaginationListSize {
		limit = &s.MaxPaginationListSize
	}

	users, err := s.repo.GetList(ctx, fromID, *limit)
	if err != nil {
		return nil, fmt.Errorf("can't add user to repo: %w", err)
	}

	return fromEntityUsers(users), nil
}

func (s *Users) Update(ctx context.Context, in UpdateUserIn) error {
	if ok := util.HasAdminRights(ctx); !ok {
		return ErrAccessDenied
	}

	user, err := in.toUserEntity()
	if err != nil {
		return fmt.Errorf("can't convert dto to user entity: %w", err)
	}

	if err := user.Validate(); err != nil {
		return fmt.Errorf("invalid user: %w", err)
	}

	err = s.repo.Update(ctx, *user)
	if err != nil {
		return fmt.Errorf("can't add user to repo: %w", err)
	}

	return nil
}

func (s *Users) DeleteByID(ctx context.Context, ID string) error {
	if ok := util.HasAdminRights(ctx); !ok {
		return ErrAccessDenied
	}

	if err := s.repo.DeleteByID(ctx, ID); err != nil {
		return fmt.Errorf("can't delete user by ID: %w", err)
	}

	return nil
}

func (s *Users) IsAdmin(ctx context.Context, username string, password string) (bool, error) {
	u, err := s.repo.GetByUsername(ctx, username)
	if err != nil {
		return false, fmt.Errorf("can't get user by username: %w", err)
	}

	hashedPass, err := hashPassword(password)
	if err != nil {
		return false, fmt.Errorf("can't hashed password: %w", err)

	}

	if isPasswordHash(u.Password, hashedPass) {
		return false, fmt.Errorf("bad password: %w", ErrAccessDenied)
	}

	return true, nil
}

func (s *Users) AddDefaultAdmin(username string, password string) error {
	hashPass, err := hashPassword(password)
	if err != nil {
		return fmt.Errorf("can't hash password: %w", err)
	}

	u := entity.User{
		ID:       "00000000-0000-0000-0000-000000000000",
		Email:    "amin@admin.com",
		Username: username,
		Password: hashPass,
		IsAdmin:  true,
	}

	if err := u.Validate(); err != nil {
		return fmt.Errorf("invalid user: %w", err)
	}

	ctx := context.Background()
	err = s.repo.Add(ctx, u)
	if err != nil {
		return fmt.Errorf("can't add user to repo: %w", err)
	}

	return nil
}

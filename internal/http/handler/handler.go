package handler

import (
	"context"

	"gitlab.com/zaur.ashurbekov/authenticator/internal/http/api"
	"gitlab.com/zaur.ashurbekov/authenticator/internal/usecase"
)

type UserUseCase interface {
	AddUser(ctx context.Context, in usecase.AddUserIn) (*usecase.User, error)
	GetByID(ctx context.Context, ID string) (*usecase.User, error)
	GetList(ctx context.Context, fromID *string, limit *int) ([]usecase.User, error)
	Update(ctx context.Context, in usecase.UpdateUserIn) error
	DeleteByID(ctx context.Context, ID string) error
}

type UserAuthenticator struct {
	users UserUseCase
}

func NewUserAuthenticator(users UserUseCase) *UserAuthenticator {
	return &UserAuthenticator{
		users: users,
	}
}

func (a *UserAuthenticator) AddUser(ctx context.Context, request api.AddUserRequestObject) (api.AddUserResponseObject, error) {
	res, err := a.users.AddUser(ctx, toAddUserDTO(request))
	if err != nil {
		return api.AddUserdefaultJSONResponse{
			Body:       toErrorResp(err),
			StatusCode: 400,
		}, err
	}

	resp := fromUserDTO(res)

	return api.AddUser200JSONResponse(resp), nil
}

func (a *UserAuthenticator) FindUserByUuid(ctx context.Context, request api.FindUserByUuidRequestObject) (api.FindUserByUuidResponseObject, error) {
	res, err := a.users.GetByID(ctx, request.Uuid)
	if err != nil {
		return api.FindUserByUuiddefaultJSONResponse{
			Body:       toErrorResp(err),
			StatusCode: 400,
		}, nil
	}

	resp := fromUserDTO(res)

	return api.FindUserByUuid200JSONResponse(resp), nil
}

func (a *UserAuthenticator) GetUsers(ctx context.Context, request api.GetUsersRequestObject) (api.GetUsersResponseObject, error) {

	var limit *int
	if request.Params.Limit != nil {
		limit = new(int)
		*limit = int(*request.Params.Limit)
	}

	res, err := a.users.GetList(ctx, request.Params.LastUuid, limit)

	if err != nil {
		return api.GetUsersdefaultJSONResponse{
			Body:       toErrorResp(err),
			StatusCode: 400,
		}, nil
	}

	resp := fromUsersListDTO(res)

	return api.GetUsers200JSONResponse(resp), nil
}

func (a *UserAuthenticator) UpdateUser(ctx context.Context, request api.UpdateUserRequestObject) (api.UpdateUserResponseObject, error) {
	err := a.users.Update(ctx, toUpdateUserDTO(request))
	if err != nil {
		return api.UpdateUserdefaultJSONResponse{
			Body:       toErrorResp(err),
			StatusCode: 400,
		}, nil
	}

	return api.UpdateUser200Response{}, nil
}

func (a *UserAuthenticator) DeleteUser(ctx context.Context, request api.DeleteUserRequestObject) (api.DeleteUserResponseObject, error) {
	err := a.users.DeleteByID(ctx, request.Uuid)
	if err != nil {
		return api.DeleteUserdefaultJSONResponse{
			Body:       toErrorResp(err),
			StatusCode: 400,
		}, nil
	}

	return api.DeleteUser200Response{}, nil
}

func toAddUserDTO(in api.AddUserRequestObject) usecase.AddUserIn {
	return usecase.AddUserIn{
		Email:    in.Body.Email,
		Username: in.Body.Username,
		Password: in.Body.Password,
		IsAdmin:  in.Body.IsAdmin,
	}
}

func toUpdateUserDTO(in api.UpdateUserRequestObject) usecase.UpdateUserIn {
	return usecase.UpdateUserIn{
		ID:       in.Uuid,
		Email:    in.Body.Email,
		Username: in.Body.Username,
		Password: in.Body.Password,
		IsAdmin:  in.Body.IsAdmin,
	}
}

func fromUserDTO(in *usecase.User) api.User {
	return api.User{
		Email:    in.Email,
		IsAdmin:  in.IsAdmin,
		Username: in.Username,
		Uuid:     in.ID,
	}
}

func fromUsersListDTO(in []usecase.User) []api.User {
	var res []api.User
	for _, v := range in {
		res = append(res, api.User{
			Email:    v.Email,
			IsAdmin:  v.IsAdmin,
			Username: v.Username,
			Uuid:     v.ID,
		})
	}

	return res
}

package handler

import (
	"gitlab.com/zaur.ashurbekov/authenticator/internal/http/api"
	"gitlab.com/zaur.ashurbekov/authenticator/internal/util"
)

func toErrorResp(err error) api.Error {
	return api.Error{
		Description: err.Error(),
		Error:       util.UnwrapError(err).Error(),
	}
}

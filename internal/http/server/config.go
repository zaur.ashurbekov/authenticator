package server

import "time"

type Config struct {
	Host            string        `envconfig:"host" default:"0.0.0.0"`
	Port            int           `envconfig:"port" default:"3000"`
	ReadTimeout     time.Duration `envconfig:"read_timeout" default:"30s"`
	WriteTimeout    time.Duration `envconfig:"write_timeout" default:"30s"`
	ShutdownTimeout time.Duration `envconfig:"shutdown_timeout" default:"5s"`
}

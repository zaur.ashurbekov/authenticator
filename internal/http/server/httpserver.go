package server

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"

	"gitlab.com/zaur.ashurbekov/authenticator/internal/http/api"

	oapimiddleware "github.com/deepmap/oapi-codegen/pkg/chi-middleware"
	chimiddleware "github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
)

type Server struct {
	server          *http.Server
	notify          chan error
	shutdownTimeout time.Duration
}

type UserAuthenticator api.StrictServerInterface

type AuthMiddleware interface {
	Auth(next http.Handler) http.Handler
}

func BuildAndRun(cfg Config, handler UserAuthenticator, auth AuthMiddleware) *Server {
	swagger, err := api.GetSwagger()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error loading swagger spec\n: %s", err)
		os.Exit(1)
	}
	swagger.Servers = nil

	Handler := api.NewStrictHandler(handler, nil)

	r := chi.NewRouter()

	r.Use(chimiddleware.Logger)
	r.Use(chimiddleware.Recoverer)
	r.Use(auth.Auth)
	r.Use(oapimiddleware.OapiRequestValidator(swagger))
	api.HandlerFromMux(Handler, r)

	server := &http.Server{
		Addr:         net.JoinHostPort(cfg.Host, strconv.Itoa(cfg.Port)),
		Handler:      r,
		ReadTimeout:  cfg.ReadTimeout,
		WriteTimeout: cfg.WriteTimeout,
	}

	s := Server{
		server:          server,
		notify:          make(chan error),
		shutdownTimeout: cfg.ShutdownTimeout,
	}

	s.start()

	return &s
}

func (s *Server) start() {
	go func() {
		s.notify <- s.server.ListenAndServe()
		close(s.notify)
	}()
}

func (s *Server) Notify() <-chan error {
	return s.notify
}

func (s *Server) Shutdown() error {
	ctx, cancel := context.WithTimeout(context.Background(), s.shutdownTimeout)
	defer cancel()

	return s.server.Shutdown(ctx)
}

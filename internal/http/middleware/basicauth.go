package middleware

import (
	"context"
	"net/http"

	"gitlab.com/zaur.ashurbekov/authenticator/internal/util"
)

type User interface {
	IsAdmin(ctx context.Context, username string, password string) (bool, error)
}

type BasicAuth struct {
	auth User
}

func NewBasicAuth(u User) BasicAuth {
	return BasicAuth{
		auth: u,
	}
}

func (b *BasicAuth) Auth(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok {
			next.ServeHTTP(w, r)
			return
		}

		ok, err := b.auth.IsAdmin(r.Context(), user, pass)
		if err != nil {
			basicAuthFailed(w)
			return
		}
		if ok {
			ctx := util.SetAdminRights(r.Context())
			r = r.WithContext(ctx)
		}

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

func basicAuthFailed(w http.ResponseWriter) {
	w.WriteHeader(http.StatusUnauthorized)
}

package entity

import (
	"errors"
	"fmt"
	"net/mail"

	"github.com/gofrs/uuid"
)

var (
	ErrInvalidUser = errors.New("invalid user")
)

type User struct {
	ID       string
	Email    string
	Username string
	Password string
	IsAdmin  bool
}

func NewUser() (User, error) {
	uuid, err := uuid.NewV6()
	if err != nil {
		return User{}, nil
	}

	return User{
		ID: uuid.String(),
	}, nil
}

func (u User) Validate() error {
	_, err := uuid.FromString(u.ID)
	if err != nil {
		return fmt.Errorf("%w: parse id: %v", ErrInvalidUser, err)
	}

	if len(u.Username) == 0 {
		return fmt.Errorf("%w: username is too short", ErrInvalidUser)
	}

	_, err = mail.ParseAddress(u.Email)
	if err != nil {
		return fmt.Errorf("%w: parse email: %v", ErrInvalidUser, err)
	}

	return nil
}

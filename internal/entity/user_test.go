package entity

import "testing"

func TestUserValidation(t *testing.T) {
	tcases := []struct {
		desc          string
		u             User
		isExpectedErr bool
	}{
		{
			desc: "positive case",
			u: User{
				ID:       "6ba7b810-9dad-11d1-80b4-00c04fd430c8",
				Email:    "sa@gma.co",
				Username: "some",
				Password: "password",
				IsAdmin:  true,
			},
			isExpectedErr: false,
		},
		{
			desc: "bad uuid",
			u: User{
				ID:       "-00c04fd430c8",
				Email:    "sa@gma.co",
				Username: "some",
				Password: "password",
				IsAdmin:  true,
			},
			isExpectedErr: true,
		},
		{
			desc: "bad email",
			u: User{
				ID:       "-00c04fd430c8",
				Email:    "gma.co",
				Username: "some",
				Password: "password",
				IsAdmin:  true,
			},
			isExpectedErr: true,
		},
		{
			desc: "bad username",
			u: User{
				ID:       "-00c04fd430c8",
				Email:    "gma.co",
				Username: "some",
				Password: "password",
				IsAdmin:  true,
			},
			isExpectedErr: true,
		},
	}

	for _, v := range tcases {
		t.Run(v.desc, func(t *testing.T) {
			err := v.u.Validate()

			if !v.isExpectedErr && err != nil {
				t.Errorf("unexpected user validation error: %v", err)
			} else if v.isExpectedErr && err == nil {
				t.Errorf("validation missed negative case")
			}

		})
	}
}

package repository

import (
	"context"
	"errors"
	"testing"

	"github.com/go-test/deep"

	"gitlab.com/zaur.ashurbekov/authenticator/internal/entity"
)

func TestAddUser(t *testing.T) {
	u := entity.User{
		ID:       "a",
		Email:    "b",
		Username: "c",
		Password: "d",
		IsAdmin:  false,
	}

	t.Run("Add new user", func(t *testing.T) {
		repo := NewUsersRepo()
		err := repo.Add(context.Background(), u)
		if err != nil {
			t.Errorf("can't add user %v to repo: %v", u, err)
			return
		}

		if len(repo.users) != 1 {
			t.Errorf("expected count of users in repo 1, got: %v", len(repo.users))
			return
		}

		if diff := deep.Equal(repo.users[0], u); diff != nil {
			t.Errorf("bad user value in repo: %v", diff)
			return
		}
	})

	t.Run("Add dublicated ID user", func(t *testing.T) {
		repo := NewUsersRepo()
		err := repo.Add(context.Background(), u)
		if err != nil {
			t.Errorf("can't add user %v to repo: %v", u, err)
		}

		u2 := u
		u2.Username = "c2"

		err = repo.Add(context.Background(), u)
		if err == nil || !errors.Is(err, ErrUserExists) {
			t.Errorf("expected err %v, got: %v", ErrUserExists, err)
		}

		if len(repo.users) != 1 {
			t.Errorf("expected count of users in repo 1, got: %v", len(repo.users))
			return
		}
	})

	t.Run("Add dublicated username user", func(t *testing.T) {
		repo := NewUsersRepo()

		err := repo.Add(context.Background(), u)
		if err != nil {
			t.Errorf("can't add user %v to repo: %v", u, err)
		}

		u2 := u
		u2.ID = "a2"

		err = repo.Add(context.Background(), u)
		if err == nil || !errors.Is(err, ErrUserExists) {
			t.Errorf("expected err %v, got: %v", ErrUserExists, err)
		}

		if len(repo.users) != 1 {
			t.Errorf("expected count of users in repo 1, got: %v", len(repo.users))
			return
		}
	})
}

func TestGetUserByID(t *testing.T) {
	u := entity.User{
		ID:       "a",
		Email:    "b",
		Username: "c",
		Password: "d",
		IsAdmin:  false,
	}

	repo := NewUsersRepo()

	err := repo.Add(context.Background(), u)
	if err != nil {
		t.Errorf("can't add user %v to repo: %v", u, err)
		return
	}

	t.Run("get user", func(t *testing.T) {
		u2, err := repo.GetByID(context.Background(), u.ID)
		if err != nil {
			t.Errorf("can't get: %v", err)
			return
		}

		if diff := deep.Equal(&u, u2); diff != nil {
			t.Errorf("bad user value return: %v", diff)
			return
		}
	})
}

func TestGetUserByUsername(t *testing.T) {
	u := entity.User{
		ID:       "a",
		Email:    "b",
		Username: "c",
		Password: "d",
		IsAdmin:  false,
	}

	repo := NewUsersRepo()

	err := repo.Add(context.Background(), u)
	if err != nil {
		t.Errorf("can't add user %v to repo: %v", u, err)
		return
	}

	t.Run("get user", func(t *testing.T) {
		u2, err := repo.GetByUsername(context.Background(), u.Username)
		if err != nil {
			t.Errorf("can't get: %v", err)
			return
		}

		if diff := deep.Equal(&u, u2); diff != nil {
			t.Errorf("bad user value return: %v", diff)
			return
		}
	})
}

func TestGetList(t *testing.T) {
	users := []entity.User{
		{
			ID:       "a1",
			Email:    "b",
			Username: "c1",
			Password: "d",
			IsAdmin:  false,
		},
		{
			ID:       "a2",
			Email:    "b",
			Username: "c2",
			Password: "d",
			IsAdmin:  false,
		},
		{
			ID:       "a3",
			Email:    "b",
			Username: "c3",
			Password: "d",
			IsAdmin:  false,
		},
	}
	repo := NewUsersRepo()

	for _, u := range users {
		err := repo.Add(context.Background(), u)
		if err != nil {
			t.Errorf("can't add user %v to repo: %v", u, err)
			return
		}
	}

	t.Run("get all users", func(t *testing.T) {
		l, err := repo.GetList(context.Background(), &users[0].ID, 3)
		if err != nil {
			t.Errorf("can't get: %v", err)
			return
		}

		if diff := deep.Equal(users, l); diff != nil {
			t.Errorf("bad users list return: %v", diff)
			return
		}
	})

	t.Run("get sub users", func(t *testing.T) {
		l, err := repo.GetList(context.Background(), &users[1].ID, 10)
		if err != nil {
			t.Errorf("can't get: %v", err)
			return
		}

		if diff := deep.Equal(users[1:], l); diff != nil {
			t.Errorf("bad users list return: %v", diff)
			return
		}
	})

	t.Run("get without fromID", func(t *testing.T) {
		l, err := repo.GetList(context.Background(), nil, 10)
		if err != nil {
			t.Errorf("can't get: %v", err)
			return
		}

		if diff := deep.Equal(users, l); diff != nil {
			t.Errorf("bad users list return: %v", diff)
			return
		}
	})
}

func TestUpdateUser(t *testing.T) {
	u := entity.User{
		ID:       "a",
		Email:    "b",
		Username: "c",
		Password: "d",
		IsAdmin:  false,
	}

	repo := NewUsersRepo()

	err := repo.Add(context.Background(), u)
	if err != nil {
		t.Errorf("can't add user %v to repo: %v", u, err)
		return
	}

	t.Run("update user", func(t *testing.T) {
		u2 := u
		u2.Username = "c2"

		err = repo.Update(context.Background(), u2)
		if err != nil {
			t.Errorf("can't update user: %v", err)
			return
		}

		if diff := deep.Equal(repo.users[0], u2); diff != nil {
			t.Errorf("bad user value in repo: %v", diff)
			return
		}
	})

	t.Run("update non-existent user", func(t *testing.T) {
		u2 := u
		u2.ID = "a2"

		err = repo.Update(context.Background(), u2)
		if err == nil || !errors.Is(err, ErrUserNotFound) {
			t.Errorf("expected err %v, got: %v", ErrUserNotFound, err)
		}
	})
}

func TestDeleteUser(t *testing.T) {
	u := entity.User{
		ID:       "a",
		Email:    "b",
		Username: "c",
		Password: "d",
		IsAdmin:  false,
	}

	repo := NewUsersRepo()

	err := repo.Add(context.Background(), u)
	if err != nil {
		t.Errorf("can't add user %v to repo: %v", u, err)
		return
	}

	t.Run("delete non-existent user", func(t *testing.T) {
		err = repo.DeleteByID(context.Background(), "a2")
		if err == nil || !errors.Is(err, ErrUserNotFound) {
			t.Errorf("expected err %v, got: %v", ErrUserNotFound, err)
		}

		if len(repo.users) != 1 {
			t.Errorf("expected count of users in repo 1, got: %v", len(repo.users))
			return
		}
	})

	t.Run("delete user", func(t *testing.T) {
		err = repo.DeleteByID(context.Background(), u.ID)
		if err != nil {
			t.Errorf("can't delete user: %v", err)
			return
		}

		if len(repo.users) != 0 {
			t.Errorf("expected count of users in repo 0, got: %v", len(repo.users))
			return
		}
	})
}

package repository

import (
	"context"
	"errors"
	"fmt"
	"sync"

	"gitlab.com/zaur.ashurbekov/authenticator/internal/entity"
)

var (
	ErrUserNotFound = errors.New("user not found")
	ErrUserExists   = errors.New("user exists")
)

type UsersRepo struct {
	users     []entity.User
	ids       map[string]int
	usernames map[string]int
	mt        sync.Mutex
}

func NewUsersRepo() *UsersRepo {
	return &UsersRepo{
		users:     []entity.User{},
		ids:       map[string]int{},
		usernames: map[string]int{},
	}
}

func (r *UsersRepo) Add(_ context.Context, u entity.User) error {
	r.mt.Lock()
	defer r.mt.Unlock()

	if _, ok := r.ids[u.ID]; ok {
		return fmt.Errorf("dublicate of ID: %w", ErrUserExists)
	}

	if _, ok := r.usernames[u.Username]; ok {
		return fmt.Errorf("dublicate of username: %w", ErrUserExists)
	}

	r.users = append(r.users, u)
	r.ids[u.ID] = len(r.users) - 1
	r.usernames[u.Username] = len(r.users) - 1

	return nil
}

func (r *UsersRepo) GetByID(_ context.Context, ID string) (*entity.User, error) {
	r.mt.Lock()
	defer r.mt.Unlock()

	i, ok := r.ids[ID]
	if !ok {
		return nil, ErrUserNotFound
	}

	return &r.users[i], nil
}

func (r *UsersRepo) GetByUsername(_ context.Context, username string) (*entity.User, error) {
	r.mt.Lock()
	defer r.mt.Unlock()

	i, ok := r.usernames[username]
	if !ok {
		return nil, ErrUserNotFound
	}

	return &r.users[i], nil
}

func (r *UsersRepo) GetList(_ context.Context, fromID *string, limit int) ([]entity.User, error) {
	r.mt.Lock()
	defer r.mt.Unlock()

	i := 0
	if fromID != nil {
		var ok bool
		i, ok = r.ids[*fromID]
		if !ok {
			return nil, ErrUserNotFound
		}
	}

	if len(r.users)-i > limit {
		return r.users[i : i+limit], nil
	}

	return r.users[i:], nil
}

func (r *UsersRepo) Update(_ context.Context, u entity.User) error {
	r.mt.Lock()
	defer r.mt.Unlock()

	userIndex, ok := r.ids[u.ID]
	if !ok {
		return ErrUserNotFound
	}

	if i, ok := r.usernames[u.Username]; ok && userIndex != i {
		return fmt.Errorf("dublicate of username: %w", ErrUserExists)
	}

	if old := r.users[userIndex].Username; old != u.Username {
		delete(r.usernames, old)
		r.usernames[u.Username] = userIndex
	}

	r.users[userIndex] = u

	return nil
}

func (r *UsersRepo) DeleteByID(_ context.Context, ID string) error {
	r.mt.Lock()
	defer r.mt.Unlock()

	userIndex, ok := r.ids[ID]
	if !ok {
		return ErrUserNotFound
	}

	delete(r.usernames, r.users[userIndex].Username)
	delete(r.ids, r.users[userIndex].ID)

	users := r.users[0:userIndex]
	if len(r.users) > userIndex+1 {
		users = append(users, r.users[userIndex+1:]...)
	}

	r.users = users

	return nil
}

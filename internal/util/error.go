package util

import "errors"

func UnwrapError(err error) error {

	for {
		if e := errors.Unwrap(err); e == nil {
			break
		} else {
			err = e
		}
	}

	return err
}

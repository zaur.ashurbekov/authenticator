package util

import (
	"fmt"
	"testing"
)

func TestUnwrapError(t *testing.T) {
	t.Run("succes case", func(t *testing.T) {
		err1 := fmt.Errorf("a1")
		err2 := fmt.Errorf("a2:%w", err1)
		err3 := fmt.Errorf("a3:%w", err2)

		if UnwrapError(err3) != err1 {
			t.Errorf("expected '%v', got '%v'", err1, UnwrapError(err3))
		}
	})

	t.Run("nil case", func(t *testing.T) {
		if UnwrapError(nil) != nil {
			t.Errorf("expected nil, got %v", UnwrapError(nil))
		}
	})

}

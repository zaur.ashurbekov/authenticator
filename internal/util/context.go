package util

import (
	"context"
)

// Custom context storage keys
const (
	hasAdminRights = "has_admin_rights"
)

func SetAdminRights(ctx context.Context) context.Context {
	ctx = context.WithValue(ctx, hasAdminRights, true)

	return ctx
}

func HasAdminRights(ctx context.Context) bool {
	hasRights := ctx.Value(hasAdminRights)
	if hasRights == nil {
		return false
	}

	if r, ok := hasRights.(bool); ok && r {
		return true
	}

	return false
}

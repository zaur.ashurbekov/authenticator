package config

import (
	"fmt"

	"gitlab.com/zaur.ashurbekov/authenticator/internal/http/server"
	"gitlab.com/zaur.ashurbekov/authenticator/internal/usecase"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

const (
	CoreEnvironmentPrefix = "authenticator"
)

// Config is a composition of all configs.
type Config struct {
	UserUsecase usecase.UserConfig `envconfig:"user_usecase" required:"true"`
	HttpServer  server.Config      `envconfig:"http_server" required:"true"`
}

// LoadConfig loads config from .env.
func LoadConfig() (Config, error) {
	cfg := Config{}

	if err := godotenv.Load(); err != nil {
		return cfg, fmt.Errorf("loading .env: %w", err)
	}
	if err := envconfig.Process(CoreEnvironmentPrefix, &cfg); err != nil {
		return cfg, fmt.Errorf("parsing config from env vars: %w", err)
	}

	return cfg, nil
}

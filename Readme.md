Запуск go run .

Возможно какие-то из ручек не работают до конца верно, уже не стал тратить на полный дебаг время

Необходимо написать веб сервис который занимается хранением  профилей пользователей и их авторизацией.
Профиль имеет набор полей:
1. id (uuid, unique)
2. email
3. username (unique)
4. password
5. admin (bool)
У сервиса должey быть набор ручек(rest, json):
/user (создвние пользователя, выдача листинга пользователей)
/user/{id} (выдача профиля по id, изменение и удаление профиля)
Сирвис использует basic access authentication (https://en.wikipedia.org/wiki/Basic_access_authentication)
Просмотр профилей могут просматривать все зарегистрированные пользователи, создавать, изменять и удалять только с пометкой admin.
Для хранения данных профилей необходимо реализовать примитивную in memory базу данных

Wikipedia (https://en.wikipedia.org/wiki/Basic_access_authentication)
Basic access authentication
method for an HTTP user agent to provide a user name and password when making a request
package cmd

import (
	"os"
	"os/signal"
	"syscall"

	"golang.org/x/exp/slog"

	"gitlab.com/zaur.ashurbekov/authenticator/config"
	"gitlab.com/zaur.ashurbekov/authenticator/internal/http/handler"
	"gitlab.com/zaur.ashurbekov/authenticator/internal/http/middleware"
	"gitlab.com/zaur.ashurbekov/authenticator/internal/http/server"
	"gitlab.com/zaur.ashurbekov/authenticator/internal/repository"
	"gitlab.com/zaur.ashurbekov/authenticator/internal/usecase"
)

func RunCore(cfg config.Config) {
	log := slog.New(
		slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
	)
	log.Info("Starting authenticator...")

	// Build and run http server
	userRepo := repository.NewUsersRepo()
	userUsecase := usecase.NewUsers(cfg.UserUsecase, userRepo)
	if err := userUsecase.AddDefaultAdmin("admin", "admin"); err != nil {
		log.Error("can't add default admin", err)
		return
	}

	userHandler := handler.NewUserAuthenticator(userUsecase)
	authMdlwr := middleware.NewBasicAuth(userUsecase)
	httpServer := server.BuildAndRun(cfg.HttpServer, userHandler, &authMdlwr)

	log.Info("Authenticator started")

	// Waiting signal
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	select {
	case s := <-interrupt:
		log.Info("interrupt signal: " + s.String())
	case err := <-httpServer.Notify():
		log.Error("http server notify error", err)
	}

	// Shutdown
	err := httpServer.Shutdown()
	if err != nil {
		log.Error("httpServer shutdown err: %w", err)
	}
}

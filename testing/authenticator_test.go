package testing

import (
	"net/url"
	"testing"

	"github.com/gavv/httpexpect/v2"
	"gitlab.com/zaur.ashurbekov/authenticator/internal/http/api"
)

const (
	host      = "localhost:3002"
	zeroAdmin = "00000000-0000-0000-0000-000000000000"
)

func TestAuthenticator_GetByID(t *testing.T) {
	u := url.URL{
		Scheme: "http",
		Host:   host,
	}
	e := httpexpect.Default(t, u.String())

	e.GET("/user/" + zeroAdmin).
		Expect().
		Status(200).
		JSON().Object().
		ContainsKey("email")
}

func TestAuthenticator_GetList(t *testing.T) {
	u := url.URL{
		Scheme: "http",
		Host:   host,
	}
	e := httpexpect.Default(t, u.String())

	e.GET("/users").
		Expect().
		Status(200).
		JSON().Array()
}

func TestAuthenticator_AddNewUser(t *testing.T) {
	u := url.URL{
		Scheme: "http",
		Host:   host,
	}
	e := httpexpect.Default(t, u.String())

	e.POST("/users").
		WithJSON(api.NewUser{
			Email:    "admin2@amind.com",
			IsAdmin:  true,
			Password: "pass2",
			Username: "username2",
		}).
		WithBasicAuth("admin", "admin").
		Expect().
		Status(200).
		JSON().Object().
		ContainsKey("email")
}

func TestAuthenticator_Delete(t *testing.T) {
	u := url.URL{
		Scheme: "http",
		Host:   host,
	}
	e := httpexpect.Default(t, u.String())

	e.DELETE("/user/" + zeroAdmin).
		Expect().
		Status(200).
		JSON().Object()
}

// func TestAuthenticator_GetByID(t *testing.T) {
// 	u := url.URL{
// 		Scheme: "http",
// 		Host:   host,
// 	}
// 	e := httpexpect.Default(t, u.String())

// 	e.POST("/url").
// 		WithJSON(ap).
// 		WithBasicAuth("admin", "admin").
// 		Expect().
// 		Status(200).
// 		JSON().Object().
// 		ContainsKey("")
// }

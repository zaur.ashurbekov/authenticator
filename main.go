package main

import (
	"log"

	"gitlab.com/zaur.ashurbekov/authenticator/cmd"
	"gitlab.com/zaur.ashurbekov/authenticator/config"
)

//todo: middlwrs access, panic, logs

func main() {
	cfg, err := config.LoadConfig()
	if err != nil {
		log.Fatalf("Config error: %s", err)
	}

	cmd.RunCore(cfg)
}
